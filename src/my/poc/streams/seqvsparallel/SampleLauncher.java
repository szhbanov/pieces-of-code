package my.poc.streams.seqvsparallel;

import java.util.List;

/**
 * Sample launcher
 */
abstract class SampleLauncher {

  void start(List<Long> list) {
    before();

    long startTime = System.currentTimeMillis();
    List<String> result = convert(list);

    after(startTime, result);
  }

  abstract String getName();

  void before() {
    System.out.println("--------------------------------------");
    System.out.println(getName());
  }

  abstract List<String> convert(List<Long> list);

  void after(long startTime, List<String> result) {
    System.out.println("Took " + (System.currentTimeMillis() - startTime) + " ms");
    System.out.println("Result has " + result.size() + " items");
  }
}
