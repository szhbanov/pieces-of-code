Split list with Stream API
=
Result:
```
original list: [1, 20, 3, 4, 12, 9, 33, 10]
list for x >= 10: [20, 12, 33, 10]
list for x < 10: [1, 3, 4, 9]
```
