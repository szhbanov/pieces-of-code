Stream API with long IO
=
Result:
```
SAMPLE LEN = 1000
sample name                                       	              result	            took, ns	comment
-----------                                       	         -----------	         -----------	-----------
Simple loop to summary values of the list         	              499500	                 613	No bad.
Long stream sum with map to LongStream            	              499500	                2459	Oh, my God! Stream API is very slow!
Long stream sum on prepared stream                	              499500	                 122	Hm... no! It is slow preparing stream and mapping to LongStream.
Stream reduce                                     	              499500	                 740	Stream Reduce gives mean result.
Parallel stream reduce                            	              499500	                3765	Fuuuu. Parallel reducing is no good idea!
Stream loop                                       	              499500	                 797	Stream Loop looks like Stream Reduce.
Parallel stream loop                              	              202424	                 785	Lost some data because of concurrent access. Don't do that! Never!
```
It looks like Steam API is no good to process data. But ... What if I increase sample data?
```
SAMPLE LEN = 10000000
sample name                                       	              result	            took, ns	comment
-----------                                       	         -----------	         -----------	-----------
Simple loop to summary values of the list         	      49999995000000	              185821
Long stream sum with map to LongStream            	      49999995000000	               32510	Best result!
Stream reduce                                     	      49999995000000	              195395
Parallel stream reduce                            	      49999995000000	              103232
Stream collect                                    	      49999995000000	               92655	Reducing creates new objects, but Collect mutates them. That is why Collect is faster!
Parallel stream collect                           	      49999995000000	               27904	More faster with parallelism!
Stream reduce with function pointer               	      49999995000000	              173830
Parallel stream reduce with function pointer      	      49999995000000	              113624
Stream loop                                       	      49999995000000	              150958	External variable in lambda prevent divide task to subtasks.
Stream loop with LongAdder accumulator            	      49999995000000	               29349	Special accumulator don't prevent Stream to be performance. Best result!

Conclusion.
Turn on your brain before using Stream API!
```
