package my.poc.streams.seqvsparallel;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * The sample to show difference between sequential and parallel stream processing
 * @author Stanislav Zhbanov
 */
public class Main {
  public static void main(String[] args) {
    // prepare data using streams
    List<Long> list = LongStream.range(1L, 100L).boxed().collect(Collectors.toList());

    new SequentialStreamLauncher().start(list);
    new CommonPoolParallelStreamLauncher().start(list);
    new CustomPoolParallelStreamLauncher().start(list);
  }
}















