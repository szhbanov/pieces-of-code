package my.poc.streams.performance;

import java.util.List;

/**
 * Stream loop
 */
class StreamLoop extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Stream loop";
  }

  @Override
  public Long calculate(List<Long> list) {
    final Long[] sum = {0L};
    list.stream().forEach(x -> sum[0] += x);
    return sum[0];
  }
}
