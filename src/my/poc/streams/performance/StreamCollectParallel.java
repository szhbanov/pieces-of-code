package my.poc.streams.performance;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Parallel stream collect
 */
class StreamCollectParallel extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Parallel stream collect";
  }

  @Override
  public Long calculate(List<Long> list) {
    return list.parallelStream().collect(Collectors.summingLong(Long::longValue));
  }
}
