Stream API with long IO
=
Result:
```
- ------------------------------------
Sequential data processing
Took 4979 ms
Result has 99 items
--------------------------------------
Parallel data processing with common pool
Common pool size is 7
Took 737 ms
Result has 99 items
--------------------------------------
Parallel data processing with custom pool
Custom pool size is 20
Took 410 ms
Result has 99 items
```
