package my.poc.streams.seqvsparallel;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**
 * Parallel data processing with common pool
 */
class CommonPoolParallelStreamLauncher extends SampleLauncher {

  @Override
  String getName() {
    return "Parallel data processing with common pool";
  }

  @Override
  void before(){
    super.before();
    System.out.println("Common pool size is " + ForkJoinPool.commonPool().getParallelism());
  }

  @Override
  List<String> convert(List<Long> list) {
    return list.parallelStream().map(Long2StringProcessor::process).collect(Collectors.toList());
  }
}
