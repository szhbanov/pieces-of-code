package my.poc.streams.performance;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * The sample to show stream performance
 * @author Stanislav Zhbanov
 */
public class Main {
  public static void main(String[] args) {
    smallSample();
    System.out.println("\r\nIt looks like Steam API is no good to process data. But ... What if I increase sample data?\r\n");
    bigSample();
    System.out.println("\r\nConclusion.\r\nTurn on your brain before using Stream API!");
  }

  private static void smallSample() {
    // prepare sample data
    List<Long> list = LongStream.range(1L, 1000L).boxed().collect(Collectors.toList());

    System.out.println("SAMPLE LEN = 1000");

    SampleLauncher.header();
    new SimpleLoop().start(list).end("No bad.");
    new LongStreamSum().start(list).end("Oh, my God! Stream API is very slow!");

    LongStream longStream =  list.stream().mapToLong(Long::longValue);
    new LongStreamSumPrepared().start(longStream).end("Hm... no! It is slow preparing stream and mapping to LongStream.");

    new StreamReduce().start(list).end("Stream Reduce gives mean result.");

    new StreamReduceParallel().start(list).end("Fuuuu. Parallel reducing is no good idea!");

    new StreamLoop().start(list).end("Stream Loop looks like Stream Reduce.");

    new StreamLoopParallel().start(list).end("Lost some data because of concurrent access. Don't do that! Never!");
  }

  private static void bigSample() {
    // prepare sample data
    List<Long> list = LongStream.range(1L, 10000000L).boxed().collect(Collectors.toList());

    System.out.println("SAMPLE LEN = 10000000");

    SampleLauncher.header();
    new SimpleLoop().start(list).end();
    new LongStreamSum().start(list).end("Best result!");

    new StreamReduce().start(list).end();
    new StreamReduceParallel().start(list).end("Hm... Parallel stream no bad. It is very usefull for hard calculations and IO.");
    new StreamCollect().start(list).end("Reducing creates new objects, but Collect mutates them. That is why Collect is faster!");
    new StreamCollectParallel().start(list).end("More faster with parallelism!");

    new StreamReducePointer().start(list).end();
    new StreamReducePointerParallel().start(list).end();

    new StreamLoop().start(list).end("External variable in lambda prevent divide task to subtasks.");
    new StreamLoopWithAccu().start(list).end("Special accumulator don't prevent Stream to be performance. Best result!");
  }
}















