package my.poc.streams.seqvsparallel;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**
 * Parallel data processing with custom pool
 */
class CustomPoolParallelStreamLauncher extends SampleLauncher {

  private static final ThreadLocal<ForkJoinPool> pools = new ThreadLocal<>();

  @Override
  String getName() {
    return "Parallel data processing with custom pool";
  }

  @Override
  void before(){
    super.before();
    ForkJoinPool pool = new ForkJoinPool(20);
    System.out.println("Custom pool size is " + pool.getParallelism());
    pools.set(pool);
  }

  @Override
  List<String> convert(List<Long> list) {

    List<String> result;
    try {
      result = pools.get().submit(() ->
          list.parallelStream().map(Long2StringProcessor::process).collect(Collectors.toList())
      ).get();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
      result = Collections.emptyList();
    }
    return result;
  }
}
