package my.poc.streams.performance;

import java.util.List;
import java.util.concurrent.atomic.LongAdder;

/**
 * Stream loop with LongAdder accumulator
 */
class StreamLoopWithAccu extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Stream loop with LongAdder accumulator";
  }

  @Override
  public Long calculate(List<Long> list) {
    LongAdder sum = new LongAdder();
    list.parallelStream().forEach(sum::add);
    return sum.longValue();
  }
}
