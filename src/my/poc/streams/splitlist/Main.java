package my.poc.streams.splitlist;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This sample shows how to split list by condition with streams
 * @author Stanislav Zhbanov
 */
public class Main {
  public static void main(String[] args) {
    List<Long> list = Arrays.asList(1L, 20L, 3L, 4L, 12L, 9L, 33L, 10L);
    Map<Boolean, List<Long>> result = list.stream().collect(Collectors.groupingBy(item -> item < 10));

    System.out.println("original list: " + list);
    System.out.println("list for x >= 10: " + result.get(false));
    System.out.println("list for x < 10: " + result.get(true));
  }
}
