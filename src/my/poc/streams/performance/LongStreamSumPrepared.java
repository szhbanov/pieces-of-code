package my.poc.streams.performance;

import java.util.stream.LongStream;

/**
 * Long stream sum on prepared stream
 */
class LongStreamSumPrepared extends SampleLauncher<LongStream> {

  @Override
  public String getName() {
    return "Long stream sum on prepared stream";
  }

  @Override
  public Long calculate(LongStream stream) {
    return stream.sum();
  }
}
