package my.poc.streams.performance;

import java.util.List;

/**
 * Simple loop to summary values of the list
 */
class SimpleLoop extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Simple loop to summary values of the list";
  }

  @Override
  public Long calculate(List<Long> list) {
    Long sum = 0L;
    for (Long item : list) {
      sum += item;
    }
    return sum;
  }
}
