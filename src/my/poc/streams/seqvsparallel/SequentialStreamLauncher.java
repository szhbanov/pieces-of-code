package my.poc.streams.seqvsparallel;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Sequential data processing
 */
class SequentialStreamLauncher extends SampleLauncher {

  @Override
  String getName() {
    return "Sequential data processing";
  }

  @Override
  List<String> convert(List<Long> list) {
    return list.stream().map(Long2StringProcessor::process).collect(Collectors.toList());
  }
}
