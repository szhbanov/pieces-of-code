package my.poc.streams.performance;

/**
 * Sample launcher
 */
abstract class SampleLauncher<T> {

  public static final String HEADER_FORMAT = "%-50s\t%20s\t%20s\t%s";
  public static final String DASH = "-----------";
  public static final String ROW_FORMAT = "%-50s\t%20d\t%20d";

  public static void header() {
    System.out.println(String.format(HEADER_FORMAT, "sample name", "result", "took, ns", "comment"));
    System.out.println(String.format(HEADER_FORMAT, DASH, DASH, DASH, DASH));
  }

  public SampleLauncher<T> start(T list) {
    before();

    long startTime = System.nanoTime();
    Long result = calculate(list);

    after(startTime, result);

    return this;
  }

  protected abstract String getName();

  public void end(String comment) {
    System.out.print("\t" + comment + "\r\n");
  }
  public void end() {
    end("");
  }

  protected void before() {
  }

  protected abstract Long calculate(T list);

  protected void after(long startTime, Long result) {
    System.out.print(String.format(ROW_FORMAT, getName(), result, (System.nanoTime() - startTime) / 1_000));
  }


}
