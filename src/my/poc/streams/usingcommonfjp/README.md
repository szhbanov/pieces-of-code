There is no side effects with using ForkJoinPool.commonPool
=
Result:
```
longTask begin
shortTask begin
shortTask complete in 7
longTask complete in 7543
```

Use *new ForkJoinPool(N)* if you afraid of using parallel streams.
