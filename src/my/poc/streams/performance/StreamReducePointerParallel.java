package my.poc.streams.performance;

import java.util.List;

/**
 * Parallel stream reduce with function pointer
 */
class StreamReducePointerParallel extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Parallel stream reduce with function pointer";
  }

  @Override
  public Long calculate(List<Long> list) {
    return list.parallelStream().reduce(0L, Long::sum);
  }
}
