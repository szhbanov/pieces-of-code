package my.poc.streams.performance;

import java.util.List;

/**
 * Stream reduce with function pointer
 */
class StreamReducePointer extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Stream reduce with function pointer";
  }

  @Override
  public Long calculate(List<Long> list) {
    return list.stream().reduce(0L, Long::sum);
  }
}
