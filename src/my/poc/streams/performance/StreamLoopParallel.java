package my.poc.streams.performance;

import java.util.List;

/**
 * Parallel stream loop
 */
class StreamLoopParallel extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Parallel stream loop";
  }

  @Override
  public Long calculate(List<Long> list) {
    final Long[] sum = {0L};
    list.parallelStream().forEach(x -> sum[0] += x);
    return sum[0];
  }
}
