package my.poc.streams.performance;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Stream collect
 */
class StreamCollect extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Stream collect";
  }

  @Override
  public Long calculate(List<Long> list) {
    return list.stream().collect(Collectors.summingLong(Long::longValue));
  }
}
