package my.poc.streams.usingcommonfjp;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * This sample shows there is no side effects with using ForkJoinPool.commonPool
 * @author Stanislav Zhbanov
 */
public class Main {
  public static void main(String[] args) {

    Runnable longTask = () -> {
      long startTime = System.currentTimeMillis();
      System.out.println("longTask begin");
      List<Long> list = LongStream.range(1L, 10000L).boxed().collect(Collectors.toList());
      list.parallelStream().map(Main::process).collect(Collectors.toList());
      System.out.println("longTask complete in " + (System.currentTimeMillis() - startTime));
    };

    Runnable shortTask = () -> {
      long startTime = System.currentTimeMillis();
      System.out.println("shortTask begin");
      List<Long> list = LongStream.of(1L).boxed().collect(Collectors.toList());
      list.parallelStream().map(Main::process).collect(Collectors.toList());
      System.out.println("shortTask complete in " + (System.currentTimeMillis() - startTime));
    };

    new Thread(longTask).start();
    try {Thread.sleep(100);} catch (InterruptedException e) {e.printStackTrace();} // pause
    new Thread(shortTask).start();
  }

  static String process(Long x) {

    try {
      Thread.sleep(6);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    return "User" + x;
  }
}
