Pieces of code
=
Some java investigations to share with my colleagues

+ [Sequential and parallel stream processing](./src/my/poc/streams/seqvsparallel)
+ [Splitting list with stream grouping](./src/my/poc/streams/splitlist)
+ [Stream API performance](./src/my/poc/streams/performance)
