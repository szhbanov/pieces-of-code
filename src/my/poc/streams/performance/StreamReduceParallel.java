package my.poc.streams.performance;

import java.util.List;

/**
 * Parallel stream reduce
 */
class StreamReduceParallel extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Parallel stream reduce";
  }

  @Override
  public Long calculate(List<Long> list) {
    return list.parallelStream().reduce(0L, (a, b) -> a + b);
  }
}
