package my.poc.streams.performance;

import java.util.List;

/**
 * Stream reduce
 */
class StreamReduce extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Stream reduce";
  }

  @Override
  public Long calculate(List<Long> list) {
    return list.stream().reduce(0L, (a, b) -> a + b);
  }
}
