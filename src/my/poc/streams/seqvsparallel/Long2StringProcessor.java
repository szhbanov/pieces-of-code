package my.poc.streams.seqvsparallel;

/**
 * Data processor (Simulates IO)
 */
interface Long2StringProcessor {
  static String process(Long x) {

    try {
      Thread.sleep(x);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    return "User" + x;
  }
}
