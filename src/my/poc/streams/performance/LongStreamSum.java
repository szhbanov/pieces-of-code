package my.poc.streams.performance;

import java.util.List;

/**
 * Long stream sum with map to LongStream
 */
class LongStreamSum extends SampleLauncher<List<Long>> {

  @Override
  public String getName() {
    return "Long stream sum with map to LongStream";
  }

  @Override
  public Long calculate(List<Long> list) {
    return list.stream().mapToLong(Long::longValue).sum();
  }
}
